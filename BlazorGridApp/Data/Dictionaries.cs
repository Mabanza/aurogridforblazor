﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorGridApp.Data
{
    public class Dictionaries
    {
        public IDictionary<string,string> Departments { get; set; }
        public IDictionary<string, string> TrainingCourses { get; set; }

        public Dictionaries()
        {
            Departments = new SortedList<string, string>();
            TrainingCourses = new Dictionary<string, string>();

            Departments.Add(new KeyValuePair<string, string>("1", "MAIN"));
            Departments.Add(new KeyValuePair<string, string>("2", "NURS"));
            Departments.Add(new KeyValuePair<string, string>("3", "LAUNDRY"));
            Departments.Add(new KeyValuePair<string, string>("4", "HOUSEHOLD"));
            Departments.Add(new KeyValuePair<string, string>("5", "IT"));
            Departments.Add(new KeyValuePair<string, string>("6", "TRAIN"));
            Departments.Add(new KeyValuePair<string, string>("7", "UNITQ"));

            TrainingCourses.Add(new KeyValuePair<string, string>("C1", "Business Hospitality"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C2", "Business Administration"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C3", "Cosmetology"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C4", "Cyber Security"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C5", "Early Childhood Education"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C6", "Gas Technician"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C7", "Graphic Design"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C8", "Heritage Carpentry"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C9", "Industrial Instrumentation"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C10", "IT Web Programming"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C11", "Marine Geomatics"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C12", "Music Arts"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C13", "Nonprofit Leadership"));
            TrainingCourses.Add(new KeyValuePair<string, string>("C14", "Public Relations"));
        }
    }
}
