#pragma checksum "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8a9858c18781ba5e30920e4e46dc1a710e2d20a2"
// <auto-generated/>
#pragma warning disable 1591
namespace BlazorGridApp.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\_Imports.razor"
using BlazorGridApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\_Imports.razor"
using BlazorGridApp.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
using AuroGridClassLibrary;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
using DTOClasses;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/GridDemo")]
    public partial class GridDemo : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3>Grid Demo</h3>\r\n\r\n");
            __builder.OpenComponent<AuroGridClassLibrary.AuroGrid<EmployeeDTO>>(1);
            __builder.AddAttribute(2, "DataSource", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 10 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                       EmployeeList

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(3, "Columns", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.List<AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass>>(
#nullable restore
#line 11 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                    LoadColumns()

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(4, "IsRowCheckable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 12 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                          true

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(5, "PageSize", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 13 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                    6

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(6, "TableCSS", "table table-condensed table-striped table-hover");
            __builder.AddAttribute(7, "TableHeaderCSS", "btn-primary");
            __builder.AddAttribute(8, "SelectedRowCSS", "bg-yellow disabled color-palette");
            __builder.AddAttribute(9, "SecurityAccessLevel", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<AuroGridClassLibrary.Classes.SecurityAccessLevels>(
#nullable restore
#line 17 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                              AuroGridClassLibrary.Classes.SecurityAccessLevels.FullAccess

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(10, "OnRowSelected", new System.Action<EmployeeDTO>(
#nullable restore
#line 18 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                           (dto) =>
          {
              //Process row selection here
          }

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(11, "OnRowChecked", new System.Action<EmployeeDTO, System.Boolean, System.Collections.Generic.List<System.String>>(
#nullable restore
#line 22 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                          (dto, isOn, allSelectedHashcodes) =>
          {
              //Process row check/uncheck here
          }

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(12, "OnRowsChecked", new System.Action<System.Collections.Generic.List<EmployeeDTO>, System.Boolean, System.Collections.Generic.List<System.String>>(
#nullable restore
#line 26 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                           (rows, isOn, allSelectedHashcodes) =>
          {
              //Process "All" rows check/uncheck here
          }

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(13, "OnModifyItem", new System.Action<EmployeeDTO, AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass, System.Object>(
#nullable restore
#line 30 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                          (dto, column, newValue) =>
          {
              //Process row modifications here. Called for every field change
              string fname= column.FieldName;
          }

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(14, "OnRowsDeleted", new System.Action<System.Collections.Generic.List<EmployeeDTO>>(
#nullable restore
#line 35 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                           (List<EmployeeDTO> deletedItems) =>
          {
              //Commit deleted rows
          }

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(15, "OnReRead", new System.Func<System.Collections.Generic.List<EmployeeDTO>>(
#nullable restore
#line 39 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                      () =>
          {
              EmployeeList = ReadEmployeeList();
              return EmployeeList;
          }

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(16, "OnSaveGridVisibility", new System.Action<System.Collections.Generic.List<AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass>>(
#nullable restore
#line 44 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                                  (modifiedColumns) =>
          {
              //Process grid visibility change.
          }

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(17, "OnBeforeAddNew", new System.Func<EmployeeDTO>(
#nullable restore
#line 48 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                            () =>
          {
              EmployeeDTO newEmployee = new EmployeeDTO();
              return newEmployee;
          }

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(18, "OnAfterAddNew", new System.Action<EmployeeDTO>(
#nullable restore
#line 53 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
                           (newItem) =>
          {
              //Process the new row adding.
          }

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 60 "C:\Development\BlazorTest\BlazorGrid\BlazorGridApp\Pages\GridDemo.razor"
       

    public List<EmployeeDTO> EmployeeList = new List<EmployeeDTO>();
    public BlazorGridApp.Data.Dictionaries DemoDictionaries = new Data.Dictionaries();

    protected override async Task OnInitializedAsync()
    {
        //Load the Employee list from remote storage
        ServiceReference1.Service1Client service1Client = new ServiceReference1.Service1Client();
        EmployeeList = ReadEmployeeList();


        await base.OnInitializedAsync();
    }

    private List<EmployeeDTO> ReadEmployeeList()
    {
        List<EmployeeDTO> result = new List<EmployeeDTO>();
        //Load the Employee list from remote storage
        ServiceReference1.Service1Client service1Client = new ServiceReference1.Service1Client();
        result = service1Client.GetEmployeeList();
        return result;
    }

    private List<AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass> LoadColumns()
    {
        List<AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass> result = new List<AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass>();
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 0,
            FieldName = "RecId",
            IsHidden = false,
            ColumnName = "#",
            Width = 80.0,
            IsSortable = false,
            IsSearchable = false,
            IsReadOnly = true
        });
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 1,
            FieldName = "FirstName",
            IsHidden = false,
            ColumnName = "First Name",
            Width = 120.0,
            IsSearchable = true
        });
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 2,
            FieldName = "LastName",
            IsHidden = false,
            ColumnName = "Last Name",
            Width = 120.0,
            IsSearchable = true
        });
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 3,
            FieldName = "EmployeeNumber",
            IsHidden = false,
            ColumnName = "Employee Number",
            Width = 120.0,
            IsSearchable = true,
            Restrictions = new AuroGridClassLibrary.Classes.AuroGridColumnRestrictionClass()
            {
                IsRequired = true,
                Hint = "Field is required"
            }
        });
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 4,
            FieldName = "DOB",
            IsHidden = false,
            ColumnName = "Birth date",
            Width = 120.0,
            Format = "dd MMM yyyy",
            Restrictions = new AuroGridClassLibrary.Classes.AuroGridColumnRestrictionClass()
            {
                IsRequired = true,
                Hint = "Field is required."
            }
        });
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 5,
            FieldName = "Department",
            IsHidden = false,
            ColumnName = "Department",
            Width = 120.0,
            LookupRepresentation = AuroGridClassLibrary.Classes.LookupRepresentations.CodeAndTitle,
            Lookup = DemoDictionaries.Departments
        });

        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 6,
            FieldName = "IsTemporary",
            IsHidden = false,
            ColumnName = "Part time",
            Width = 120.0

        });
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 7,
            FieldName = "TC",
            IsHidden = false,
            ColumnName = "Training Course",
            Width = 120.0,
            LookupRepresentation = AuroGridClassLibrary.Classes.LookupRepresentations.TitleOnly,
            Lookup = DemoDictionaries.TrainingCourses
        });
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 8,
            FieldName = "Rate",
            IsHidden = false,
            ColumnName = "Rate",
            Width = 120.0,
            Format = "{0:C}",
            ColumnAlignment = AuroGridClassLibrary.Classes.ColumnAlignments.Right
        });
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 9,
            FieldName = "BankDays",
            IsHidden = false,
            ColumnName = "Days in bank",
            Width = 120.0,
            ColumnAlignment = AuroGridClassLibrary.Classes.ColumnAlignments.Middle
        });
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 10,
            FieldName = "ShiftStartTime",
            IsHidden = false,
            ColumnName = "Shift start at",
            Width = 120.0,
            ColumnAlignment = AuroGridClassLibrary.Classes.ColumnAlignments.Middle,
            DateTimeRepresentation = AuroGridClassLibrary.Classes.DateTimeRepresentations.TimeOnly,
            Format = "MM/dd/yyyy hh:mm t",
        });
        result.Add(new AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass()
        {
            ColumnIndex = 11,
            FieldName = "Email",
            IsHidden = false,
            ColumnName = "email",
            Width = 120.0,
            ColumnAlignment = AuroGridClassLibrary.Classes.ColumnAlignments.Right,
            Restrictions = new AuroGridClassLibrary.Classes.AuroGridColumnRestrictionClass()
            {
                RegularExpression = @"^[A-z0-9\.\-]+@[A-z0-9\-]+\.[A-z]+$",
                Hint = "Please enter a valid email address"
            }
        });

        return result;
    }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
