﻿using System;

namespace DTOClasses
{
    public class EmployeeDTO
    {
        public int RecId { get; set; }
        public string EmployeeNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Department { get; set; }
        public DateTime DOB { get; set; }
        public double Rate { get; set; }
        public bool IsTemporary { get; set; }
        public string TC { get; set; }
        public int BankDays { get; set; }
        public DateTime ShiftStartTime { get; set; }
        public string Email { get; set; }
    }
}
