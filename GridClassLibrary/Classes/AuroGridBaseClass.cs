﻿//-------------------------Nick Renziglov 2020. Documentation:-----------------------------------
//https://gitlab.com/Mabanza/aurogridforblazor/-/blob/dev/AuroGridClassLibrary/AuroGrid.pdf 
//-----------------------------------------------------------------------------------------------
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Reflection;
using System.Threading.Tasks;

namespace AuroGridClassLibrary.Classes
{
    public enum SecurityAccessLevels
    {
        NoAccess = 0,
        ViewOnly,
        ViewModify,
        FullAccess
    }
    public class AuroGridBaseClass<T> : ComponentBase
    {
        public PaginationClass<T> Pager;
        public FilterClass<T> Filter;
        public string TableId;
        public AuroGridBaseClass()
        {
            Filter = new FilterClass<T>();
            Pager = new PaginationClass<T>();
            Pager.AssignFilter(Filter);
            TableId = Guid.NewGuid().ToString().Split('-')[0];
            CheckedRowHashcodes = new List<string>();
            DeletedRowHashcodes = new List<string>();
        }

        public const string SelectRowWarning = "Please select a row.";

        [Microsoft.AspNetCore.Components.Parameter]
        public SecurityAccessLevels SecurityAccessLevel { get; set; }
        public bool InEditMode { get; set; }
        [Inject]
        public IJSRuntime jsRuntime { get; set; }

        [Microsoft.AspNetCore.Components.Parameter]
        public List<AuroGridColumnPropertyClass> Columns { get; set; }
        public List<AuroGridColumnPropertyClass> ColumnsGUI
        {
            get
            {
                //It returns sorted and filtered by "IsHidded" set of columns
                return Columns.Where(p => !(p.IsHidden ?? false)).OrderBy(p => p.ColumnIndex).ToList();
            }
        }

        protected List<string> CheckedRowHashcodes { get; set; }
        protected List<string> DeletedRowHashcodes { get; set; }
        private AuroGridColumnPropertyClass draggedColumn;

        private object _dataSource;
        [Microsoft.AspNetCore.Components.Parameter]
        public object DataSource 
        { 
            get 
            { 
                return _dataSource; 
            } 
            set
            {
                _dataSource = value;
                CheckedRowHashcodes.Clear();
                DeletedRowHashcodes.Clear();
                Filter.Clear();
                if ((Pager != null) && (DataSource != null)) Pager.CalculateMaxPageNumber((List<T>)DataSource);
            }
        }
        [Microsoft.AspNetCore.Components.Parameter]
        public bool IsRowCheckable { get; set; }
        [Microsoft.AspNetCore.Components.Parameter]
        public int PageSize
        {
            get { return Pager.PageSize; }
            set 
            { 
                Pager.PageSize = value;
                if (DataSource != null) Pager.CalculateMaxPageNumber((List<T>)DataSource);
            }
        }
        private string _tableCSS;
        [Microsoft.AspNetCore.Components.Parameter]
        public string TableCSS
        {
            get { return _tableCSS; }
            set
            {
                _tableCSS = value;
            }
        }
        private string _tableHeaderCSS;
        [Microsoft.AspNetCore.Components.Parameter]
        public string TableHeaderCSS
        {
            get { return _tableHeaderCSS; }
            set
            {
                _tableHeaderCSS = value;
            }
        }
        private string _selectedRowCSS;
        [Microsoft.AspNetCore.Components.Parameter]
        public string SelectedRowCSS
        {
            get { return _selectedRowCSS; }
            set { _selectedRowCSS = value; }
        }

        private string searchClause;

        private string currentRowId; //<-- HashCode of recently clicked row (selected row)
        [Microsoft.AspNetCore.Components.Parameter]
        public T CurrentRow { get; set; }
        protected T NewRow { get; set; }

        protected string validationSummaryCSS = "d-none";
        protected List<string> validationSummaryMessages;

        public List<T> GetItems()
        {
            if (DataSource == null) return new List<T>();
            return Pager.Paginate(DataSource);
        }

        [Microsoft.AspNetCore.Components.Parameter]
        public Action<T> OnRowSelected { get; set; }
        [Microsoft.AspNetCore.Components.Parameter]
        public Action<List<T>> OnRowsDeleted { get; set; }
        [Microsoft.AspNetCore.Components.Parameter]
        public Action<T, bool, List<string>> OnRowChecked { get; set; }
        [Microsoft.AspNetCore.Components.Parameter]
        public Action<List<T>, bool, List<string>> OnRowsChecked { get; set; }

        [Microsoft.AspNetCore.Components.Parameter]
        public Func<List<T>> OnReRead { get; set; }
        [Microsoft.AspNetCore.Components.Parameter]
        public Action<List<AuroGridColumnPropertyClass>> OnSaveGridVisibility { get; set; }
        [Microsoft.AspNetCore.Components.Parameter]
        public Func<T> OnBeforeAddNew { get; set; }
        [Microsoft.AspNetCore.Components.Parameter]
        public Action<T> OnAfterAddNew { get; set; }
        [Microsoft.AspNetCore.Components.Parameter]
        public Action<List<AuroGridColumnPropertyClass>> OnColumnReordered { get; set; }

        public List<AuroGridColumnPropertyClass> GetColumnProperties()
        {
            List<AuroGridColumnPropertyClass> result = new List<AuroGridColumnPropertyClass>();
            if (Columns == null)
            {
                int count = 0;
                BindingFlags bflags = BindingFlags.Public | BindingFlags.Instance;
                System.Reflection.PropertyInfo[] pii = typeof(T).GetProperties(bflags);
                foreach (System.Reflection.PropertyInfo pi in pii)
                {
                    AuroGridColumnPropertyClass item = new AuroGridColumnPropertyClass();
                    item.ColumnIndex = count;
                    item.ColumnName = item.FieldName = pi.Name;
                    result.Add(item);
                    count++;
                }
                Columns = result;
            }
            return ColumnsGUI.ToList(); 
        }

        public string GetDropdownString(LookupRepresentations representation, KeyValuePair<string, string> kvp)
        {
            return DropdownListRepresentationClass.GetDropdownString(representation, kvp);
        }

        protected bool IsCheckboxAllChecked { get; set; }

        public string GetRowHashCode(T item)
        {
            return (item!=null) ? String.Concat(TableId,"_ROW", item.GetHashCode()) : String.Empty;
        }

        public void CheckboxClicked(T item, object checkedValue)
        {
            string hcode = GetRowHashCode(item);
            switch ((bool)checkedValue)
            {
                case false:
                    if (CheckedRowHashcodes.Contains(hcode)) CheckedRowHashcodes.Remove(hcode);
                    break;
                case true:
                    if (!CheckedRowHashcodes.Contains(hcode)) CheckedRowHashcodes.Add(hcode);
                    break;
            }
            OnRowChecked?.Invoke(item, (bool)checkedValue, CheckedRowHashcodes);
        }
        public void CheckboxAllClicked(object checkedValue)
        {
            IsCheckboxAllChecked = (bool)checkedValue;
            List<T> items = Pager.Paginate(DataSource);
            foreach (T item in items) CheckboxClicked(item, checkedValue);
            OnRowsChecked?.Invoke(items, (bool)checkedValue, CheckedRowHashcodes);
        }

        public void UncheckAll()
        {
            CheckedRowHashcodes.Clear();
            IsCheckboxAllChecked = false;
        }

        public void Sort(object sender, string columnName, bool isDescending)
        {
            DataSource = !isDescending
                ? ((List<T>)DataSource).OrderBy(p => p.GetType().GetProperty(columnName).GetValue(p, null)).ToList()
                : ((List<T>)DataSource).OrderByDescending(p => p.GetType().GetProperty(columnName).GetValue(p, null)).ToList();
        }

        public void OnSearchChanged(ChangeEventArgs args)
        {
            searchClause = args.Value.ToString();
        }

        public bool IsRowChecked(T item)
        {
            return CheckedRowHashcodes.Contains(GetRowHashCode(item));
        }

        public void Search(object sender, string columnName)
        {
            if (String.IsNullOrEmpty(searchClause)) return;
            string s = searchClause.ToUpper();
            T item = ((List<T>)DataSource)
                .FirstOrDefault(p => p.GetType().GetProperty(columnName).GetValue(p, null).ToString().ToUpper().StartsWith(s));
            locateRow(item);
        }
        private void locateRow(T item)
        {
            if (item != null)
            {
                string rowHashCode = GetRowHashCode(item);

                int _page = Pager.FindPage(DataSource, forItem: item);

                //Scroll HTML table into view of the row found
                jsRuntime.InvokeVoidAsync("scrollRowIntoView", rowHashCode);
                clickRow(new Microsoft.AspNetCore.Components.Web.MouseEventArgs(), item);
            }
        }

        protected void showBrowserAlert(string stext)
        {
            jsRuntime.InvokeVoidAsync("alert", stext);
        }

        protected void clickRow(Microsoft.AspNetCore.Components.Web.MouseEventArgs args, T item)
        {
            if (args != null)
            {
                string hcode = GetRowHashCode(item);
                //applySelectedRowCSS(hcode, 30);
                currentRowId = hcode;
                CurrentRow = item;
                OnRowSelected?.Invoke(item);
            }
        }
        //private void applySelectedRowCSS(string rowHashcode, int delay)
        //{
        //    string cssclass = String.IsNullOrEmpty(SelectedRowCSS) ? "text-yellow" : SelectedRowCSS;
        //    jsRuntime.InvokeVoidAsync("selectDataGridRow", TableId, rowHashcode, cssclass, delay);
        //}

        protected void PagerPanelClick(int navigateToPage)
        {
            int i = 0;
            IsCheckboxAllChecked = false;
            switch (navigateToPage)
            {
                case -2:
                    //Move to the very first page from any Pager location
                    Pager.PagerPanelLeftButtonNumber = 1;
                    Pager.CurrentPageNumber = 1;
                    break;
                case -1:
                    //Move a group PagerPanelButtons LEFT
                    i = Pager.PagerPanelLeftButtonNumber - Pager.PagerPanelButtons;
                    if (i <= 0) i = 1;
                    Pager.PagerPanelLeftButtonNumber = i;
                    Pager.CurrentPageNumber = i;
                    break;
                case 999:
                    //Move a group PagerPanelButtons RIGHT
                    i = Pager.PagerPanelLeftButtonNumber + Pager.PagerPanelButtons;
                    if (i <= Pager.MaxPageNumber)
                    {
                        Pager.CurrentPageNumber = i;
                        Pager.PagerPanelLeftButtonNumber = i;
                    }
                    //StateHasChanged();
                    break;
                default:
                    if (navigateToPage <= Pager.MaxPageNumber) Pager.CurrentPageNumber = navigateToPage;
                    break;
            }
            
            if (!String.IsNullOrEmpty(currentRowId))
            {
                //Redraw Selected row in SelectedRowCSS class
                string istr = GetItems().Any(p => GetRowHashCode(p) == currentRowId)
                    ? currentRowId
                    : "";
                
                //applySelectedRowCSS(istr, 400);

            }
            //StateHasChanged();
        }

        protected void dblClick(T item, AuroGridColumnPropertyClass column)
        {
            if (SecurityAccessLevel>SecurityAccessLevels.ViewOnly) InEditMode = true;
            //jsRuntime.InvokeVoidAsync("alert", String.Concat(GetRowHashCode(item), " ", column.FieldName));
        }

        //protected void FilterRows(AuroGridColumnPropertyClass column, object arg)
        //{
        //    string s = "";
        //}

        protected void ResetFilter(AuroGridColumnPropertyClass forColumn)
        {
            if (forColumn == null) Filter.Clear();
            //Jump to the very first page of pager
            PagerPanelClick(-2);
            GetItems();
        }

        protected async Task LookupFilterChanged(AuroGridColumnPropertyClass column, ChangeEventArgs arg)
        {
            await Task.Run(() =>
            {
                var c = Filter.Columns.FirstOrDefault(p => p.Column == column);
                if (c != null) Filter.Columns.Remove(c);

                if (arg.Value != null)
                {
                    FilteredColumnClass fitem = new FilteredColumnClass()
                    {
                        Column = column,
                        Value = arg.Value.ToString()
                    };
                    Filter.Columns.Add(fitem);
                }

                //Since filter has changed, jump to the very first page of pager
                PagerPanelClick(-2);
            });
        }

        protected string GetRowCSSClass(string rowHashcode)
        {
            string result = String.Empty;
            if (DeletedRowHashcodes.Contains(rowHashcode)) result = "strikeout ";
            if (GetRowHashCode(CurrentRow) == rowHashcode)
            {
                //applySelectedRowCSS(rowHashcode, 30);
                string cssclass = String.IsNullOrEmpty(SelectedRowCSS) ? "text-yellow" : SelectedRowCSS;
                result += cssclass;

            }
            return result;
        }

        protected async Task DeleteRow()
        {
            if (CurrentRow == null) return;
            await Task.Run(() =>
            {
                string rowHashcode = GetRowHashCode(CurrentRow);
                bool isOn = !DeletedRowHashcodes.Contains(rowHashcode);
                //jsRuntime.InvokeVoidAsync("strikeoutDataGridRow", TableId, rowHashcode, isOn, 250);
                if (isOn) DeletedRowHashcodes.Add(rowHashcode);
                else DeletedRowHashcodes.Remove(rowHashcode);
            });
        }

        protected void CommitDeletedRows()
        {
            if (DeletedRowHashcodes.Count == 0) return;
            List<T> items = (List<T>)DataSource;
            List<T> deletedItems = new List<T>();
            foreach (string rowHashcode in DeletedRowHashcodes)
            {
                T item = items.FirstOrDefault(p => GetRowHashCode(p) == rowHashcode);
                if (item != null)
                {
                    items.Remove(item);
                    deletedItems.Add(item);
                }
            }

            OnRowsDeleted?.Invoke(deletedItems);
            DeletedRowHashcodes.Clear();

            jsRuntime.InvokeVoidAsync("alert", String.Concat(deletedItems.Count, " item(s) removed."));

            StateHasChanged();
        }

        protected void ReReadDataSource()
        {
            DataSource = OnReRead?.Invoke();
        }

        protected void ShowFieldVisibilityDialog()
        {
            foreach (AuroGridColumnPropertyClass citem in Columns) citem.IsModified = false;
            jsRuntime.InvokeVoidAsync("showFieldVisibilityDialog");
        }

        protected void VisibilitySaveClick()
        {
            List<AuroGridColumnPropertyClass> items = Columns.Where(p => p.IsModified).ToList();
            if ((items.Count > 0) && (OnSaveGridVisibility != null)) OnSaveGridVisibility(items);
            foreach (AuroGridColumnPropertyClass citem in Columns) citem.IsModified = false;
            jsRuntime.InvokeVoidAsync("hideFieldVisibilityDialog");

        }

        protected void ShowAddNewRowDialog()
        {
            if (OnBeforeAddNew == null) return;
            NewRow = OnBeforeAddNew();
            jsRuntime.InvokeVoidAsync("showAddNewRowDialog");
        }
        protected void NewRowSaveClick()
        {
            //Check field validation
            validationSummaryMessages = new List<string>();
            validationSummaryCSS = "d-none";
            AuroGridClassLibrary.Classes.CellMatcherClass<T> matcher = new CellMatcherClass<T>();
            BindingFlags bflags = BindingFlags.Public | BindingFlags.Instance;
            System.Reflection.PropertyInfo[] pii = typeof(T).GetProperties(bflags);
            foreach (AuroGridColumnPropertyClass citem in ColumnsGUI)
            {
                PropertyInfo pi = pii.FirstOrDefault(p=>p.Name==citem.FieldName);
                if (pi == null) continue;

                object o = pi.GetValue(NewRow, null);
                if (!matcher.MatchValidation[pi.PropertyType].Invoke(NewRow, citem, o))
                {
                    validationSummaryMessages.Add(String.Concat(citem.ColumnName, ": ",
                        !String.IsNullOrEmpty(citem.Restrictions.Hint)
                            ? citem.Restrictions.Hint
                            : "Invalid field value"));
                }
            }
            if (validationSummaryMessages.Count>0)
            {
                validationSummaryCSS = "";
                StateHasChanged();
                return;
            }

            //Validation successful. Add a row to the list
            if ((DataSource==null) || (((List<T>)DataSource).Count==0)) DataSource = new List<T>();
            ((List<T>)DataSource).Add(NewRow);
            locateRow(NewRow);
            if (OnAfterAddNew != null) OnAfterAddNew(NewRow);

            jsRuntime.InvokeVoidAsync("hideAddNewRowDialog");
        }

        protected void OnColumnDragStart(DragEventArgs args, AuroGridColumnPropertyClass citem)
        {
            draggedColumn = citem;
        }
        protected async Task OnColumnDragEnd(DragEventArgs args, AuroGridColumnPropertyClass citem)
        {
            if ((draggedColumn == null) || (draggedColumn == citem)) return;
            await Task.Run(() =>
            {
                //Replace columns
                int i = Columns.IndexOf(citem);
                Columns.Remove(draggedColumn);
                Columns.Insert(i, draggedColumn);
                for (int j = 0; j < Columns.Count; j++) Columns[j].ColumnIndex = j;
                draggedColumn = null;
                OnColumnReordered?.Invoke(Columns);
            });
        }
    }
}
 