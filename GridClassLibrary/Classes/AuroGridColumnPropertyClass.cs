﻿//-------------------------Nick Renziglov 2020. Documentation:-----------------------------------
//https://gitlab.com/Mabanza/aurogridforblazor/-/blob/dev/AuroGridClassLibrary/AuroGrid.pdf 
//-----------------------------------------------------------------------------------------------
using System.Collections.Generic;

namespace AuroGridClassLibrary.Classes
{
    public enum LookupRepresentations
    {
        CodeOnly=0,
        TitleOnly,
        CodeAndTitle
    }
    public enum ColumnAlignments
    {
        Left = 0,
        Middle,
        Right
    }
    public enum DateTimeRepresentations
    {
        DateOnly = 0,
        DateAndTime,
        TimeOnly
    }
    public class AuroGridColumnRestrictionClass
    {
        public int MinValue { get; set; } = int.MinValue; //<-- for numeric columns only
        public int MaxValue { get; set; } = int.MaxValue; //<-- for numeric columns only
        public string RegularExpression { get; set; } //<-- for string columns only
        public string Hint { get; set; }
        public bool IsRequired { get; set; }
    }
    public class AuroGridColumnPropertyClass
    {
        public string FieldName { get; set; }
        public string ColumnName { get; set; }
        public bool? IsHidden { get; set; }
        public int ColumnIndex { get; set; }
        public double Width { get; set; }
        public string Format { get; set; }
        public bool IsSortable { get; set; }
        public bool IsSearchable { get; set; }
        public bool IsReadOnly { get; set; }
        public bool CanDropdown
        {
            get
            {
                return IsSortable || IsSearchable;
            }
        }

        public IDictionary<string, string> Lookup { get; set; }
        public LookupRepresentations LookupRepresentation { get; set; }
        public ColumnAlignments ColumnAlignment { get; set; }
        public string ColumnAlignmentAsString 
        { 
            get 
            {
                string result = "text-left";
                switch (ColumnAlignment)
                {
                    case ColumnAlignments.Middle: result = "text-center"; break;
                    case ColumnAlignments.Right: result = "text-right"; break;
                }
                return result;
            } 
        }

        public DateTimeRepresentations DateTimeRepresentation { get; set; } //<-- For DateTime cells only
        public AuroGridColumnRestrictionClass Restrictions { get; set; }
        public bool IsModified { get; set; }

        public AuroGridColumnPropertyClass()
        {
            IsSortable = true;
            Restrictions = new AuroGridColumnRestrictionClass();
        }
    }
}
