﻿//-------------------------Nick Renziglov 2020. Documentation:-----------------------------------
//https://gitlab.com/Mabanza/aurogridforblazor/-/blob/dev/AuroGridClassLibrary/AuroGrid.pdf 
//-----------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;

namespace AuroGridClassLibrary.Classes
{
    //This class match the DTO member ordinal type to a appropriate cell component
    //Example: integer maps to IntegerEditCell, double to NumericEditCell, etc.
    public class CellMatcherClass<T>
    {
        public Dictionary<Type, Type> Matches = new Dictionary<Type, Type>
        {
            {typeof(string),typeof(AuroGridClassLibrary.GridCells.TextCell<T>)},
            {typeof(Boolean),typeof(AuroGridClassLibrary.GridCells.CheckBoxCell<T>)},
            {typeof(DateTime),typeof(AuroGridClassLibrary.GridCells.TextCell<T>)},
            {typeof(int),typeof(AuroGridClassLibrary.GridCells.TextCell<T>)},
            {typeof(double),typeof(AuroGridClassLibrary.GridCells.TextCell<T>)},
            {typeof(float),typeof(AuroGridClassLibrary.GridCells.TextCell<T>)},
            {typeof(IDictionary<string,string>),typeof(AuroGridClassLibrary.GridCells.DropDownListCell<T>)},
            {typeof(SortedList<string, string>),typeof(AuroGridClassLibrary.GridCells.DropDownListCell<T>)},
            {typeof(Dictionary<string, string>),typeof(AuroGridClassLibrary.GridCells.DropDownListCell<T>)}
        };
        public Dictionary<Type, Type> MatchesEdit = new Dictionary<Type, Type>
        {
            {typeof(string),typeof(AuroGridClassLibrary.GridCells.TextEditCell<T>)},
            {typeof(Boolean),typeof(AuroGridClassLibrary.GridCells.CheckBoxEditCell<T>)},
            {typeof(DateTime),typeof(AuroGridClassLibrary.GridCells.DateEditCell<T>)},
            {typeof(int),typeof(AuroGridClassLibrary.GridCells.IntegerEditCell<T>)},
            {typeof(double),typeof(AuroGridClassLibrary.GridCells.NumericEditCell<T>)},
            {typeof(float),typeof(AuroGridClassLibrary.GridCells.NumericEditCell<T>)},
            {typeof(IDictionary<string,string>),typeof(AuroGridClassLibrary.GridCells.DropDownListEditCell<T>)},
            {typeof(SortedList<string, string>),typeof(AuroGridClassLibrary.GridCells.DropDownListEditCell<T>)},
            {typeof(Dictionary<string, string>),typeof(AuroGridClassLibrary.GridCells.DropDownListEditCell<T>)}
        };

        public Dictionary<Type, Func<T, AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass, object, bool>> MatchValidation 
            = new Dictionary<Type, Func<T, AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass, object, bool>>
        {
            {
                    typeof(string),
                    (item, citem, value) =>
                    {
                        if (value==null) value="";
                        bool result = citem.Restrictions.IsRequired 
                            ? !String.IsNullOrEmpty(value.ToString()) 
                            : true;
                        if (result && !String.IsNullOrEmpty(citem.Restrictions.RegularExpression))
                        {
                            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(citem.Restrictions.RegularExpression);
                            if (!reg.IsMatch(value.ToString())) result=false;
                        }
                        return result;
                    }
            },
            {
                    typeof(Boolean),
                    (item, citem, value) => {return true; }
            },
            {
                    typeof(DateTime),
                    (item, citem, value) => 
                    {
                        bool result = citem.Restrictions.IsRequired
                            ? (value!=null) && !String.IsNullOrEmpty(value.ToString())
                            : true;
                        DateTime DT = DateTime.MinValue;
                        if (!result || !DateTime.TryParse(value.ToString(), out DT)) result=false;
                        if (citem.Restrictions.IsRequired && (DT==DateTime.MinValue)) result=false;
                        return result; 
                    }
             },
            {
                    typeof(int),
                    (item, citem, value) =>
                    {
                        bool result = citem.Restrictions.IsRequired
                            ? (value!=null) && !String.IsNullOrEmpty(value.ToString())
                            : true;
                        int i = 0;
                        if (!result || !int.TryParse(value.ToString(), out i) ||
                                (i<citem.Restrictions.MinValue) ||
                                (i>citem.Restrictions.MaxValue)) 
                            result=false;
                        return result;
                    }
            },
            {
                    typeof(double),
                    (item, citem, value) =>
                    {
                        bool result = true;
                        double d = 0;
                        if (!double.TryParse(value.ToString(), out d)) result=false;
                        return result;
                    }
            },
            {
                    typeof(float),
                    (item, citem, value) =>
                    {
                        bool result = true;
                        double d = 0;
                        if (!double.TryParse(value.ToString(), out d)) result=false;
                        return result;
                    }
            },
            {
                    typeof(IDictionary<string,string>),
                    (item, citem, value) => {return (value!=null) && !String.IsNullOrEmpty(value.ToString()); }
            },
            {
                    typeof(SortedList<string, string>),
                    (item, citem, value) => {return (value!=null) && !String.IsNullOrEmpty(value.ToString()); }
            },
            {
                    typeof(Dictionary<string, string>),
                    (item, citem, value) => {return (value!=null) && !String.IsNullOrEmpty(value.ToString()); }
            }
        };

    }
}
