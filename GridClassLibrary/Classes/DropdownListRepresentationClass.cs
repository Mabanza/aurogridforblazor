﻿//-------------------------Nick Renziglov 2020. Documentation:-----------------------------------
//https://gitlab.com/Mabanza/aurogridforblazor/-/blob/dev/AuroGridClassLibrary/AuroGrid.pdf 
//-----------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace AuroGridClassLibrary.Classes
{
    public static class DropdownListRepresentationClass
    {
        public static string GetDropdownString(LookupRepresentations representation, KeyValuePair<string, string> kvp)
        {
            string result = kvp.Key;
            switch (representation)
            {
                case Classes.LookupRepresentations.CodeAndTitle:
                    result = String.Concat(kvp.Key, "-", kvp.Value);
                    break;
                case Classes.LookupRepresentations.TitleOnly:
                    result = kvp.Value;
                    break;
            }
            return result;
        }

    }
}
