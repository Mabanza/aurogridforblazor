﻿//-------------------------Nick Renziglov 2020. Documentation:-----------------------------------
//https://gitlab.com/Mabanza/aurogridforblazor/-/blob/dev/AuroGridClassLibrary/AuroGrid.pdf 
//-----------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;

namespace AuroGridClassLibrary.Classes
{
    public class FilteredColumnClass
    {
        public AuroGridColumnPropertyClass Column;
        public string Value;
    }
    public class FilterClass<T>
    {
        public List<FilteredColumnClass> Columns;
        public FilterClass()
        {
            Columns = new List<FilteredColumnClass>(); 

        }

        public void Clear()
        {
            Columns.Clear();
        }

        public List<T> ApplyFilter(List<T> items)
        {
            List<T> result = items;
            if (Columns.Count > 0)
            {
                IEnumerable<T> tmpItems = result;
//Stopwatch sw = new Stopwatch();
//sw.Start();
                //Parallel.ForEach(Columns.Where(p => !String.IsNullOrEmpty(p.Value)), fcolumn =>
                //{
                //    tmpItems = tmpItems.Where(p => p.GetType()
                //                                     .GetProperty(fcolumn.Column.FieldName)
                //                                     .GetValue(p, null).ToString() == fcolumn.Value);
                //});
                foreach (FilteredColumnClass fcolumn in Columns.Where(p => !String.IsNullOrEmpty(p.Value)))
                {
                    tmpItems = tmpItems.Where(p => p.GetType()
                                                     .GetProperty(fcolumn.Column.FieldName)
                                                     .GetValue(p, null).ToString() == fcolumn.Value);
                }

//sw.Stop();
//Debug.WriteLine(sw.Elapsed.ToString());

                result = tmpItems.ToList();

            }

            return result;
        }
    }
}
