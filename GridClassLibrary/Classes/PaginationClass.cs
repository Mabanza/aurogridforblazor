﻿//-------------------------Nick Renziglov 2020. Documentation:-----------------------------------
//https://gitlab.com/Mabanza/aurogridforblazor/-/blob/dev/AuroGridClassLibrary/AuroGrid.pdf 
//-----------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AuroGridClassLibrary.Classes
{
    public class PaginationClass<T>// where T : new()
    {
        private const int defaultPageSize = 10;
        private int _pageSize;
        public int PageSize
        {
            get { return _pageSize; }
            set 
            { 
                _pageSize = value;
                if (_pageSize <= 0) _pageSize = defaultPageSize;
            }
        }

        private int _currentPage;
        public int CurrentPageNumber
        {
            get { return _currentPage; }
            set { _currentPage = value; }
        }

        //public int PrevPageNumber
        //{
        //    get 
        //    { 
        //        return _currentPage; 
        //    }

        //}

        private int _maxPageNumber;
        public int MaxPageNumber
        {
            get { return _maxPageNumber; }
            set { _maxPageNumber = value; }
        }

        public int PagerPanelButtons { get; set; }
        public int PagerPanelLeftButtonNumber { get; set; }
        private FilterClass<T> filter;

        public PaginationClass()
        {
            PageSize = defaultPageSize;
            CurrentPageNumber = 1;
            PagerPanelButtons = 6;
            PagerPanelLeftButtonNumber = 1;
        }

        public void AssignFilter(FilterClass<T> rowFilter)
        {
            filter = rowFilter;
        }

        public void CalculateMaxPageNumber(List<T> items)
        {
            MaxPageNumber = 0;
            if (items != null)
            {
                if (filter.Columns.Count > 0) items = filter.ApplyFilter(items);
                MaxPageNumber = items.Count / PageSize;
                if ((items.Count % PageSize) > 0) MaxPageNumber++;
            }
        }

        public List<T> Paginate(object dataSource)
        {
            List<T> result = (List<T>)dataSource;
            if (result == null) return result;

            if (filter.Columns.Count>0)
            {
                //Filter rows out
                result = filter.ApplyFilter(result);
            }

            CalculateMaxPageNumber(result);

            result = result.Skip((CurrentPageNumber - 1) * PageSize).Take(PageSize).ToList();

            return result;
        }

        public int FindPage(object dataSource, T forItem)
        {
            List<T> list = (List<T>)dataSource;
            int k = 0;
            int result = list.IndexOf(forItem);
            if (result>=0)
            {
                k = result / PageSize;
                if (result % PageSize > 0) k++;
                if (k > MaxPageNumber) k = MaxPageNumber;
            }

            if (k>=1)
            {
                result = k;
                CurrentPageNumber = result;
                int i = CurrentPageNumber / PagerPanelButtons;
                PagerPanelLeftButtonNumber = i * PagerPanelButtons + 1;
            }

            return result;
        }
    }
}
