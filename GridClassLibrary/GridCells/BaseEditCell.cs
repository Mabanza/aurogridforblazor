﻿//-------------------------Nick Renziglov 2020. Documentation:-----------------------------------
//https://gitlab.com/Mabanza/aurogridforblazor/-/blob/dev/AuroGridClassLibrary/AuroGrid.pdf 
//-----------------------------------------------------------------------------------------------
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuroGridClassLibrary.GridCells
{
    public class BaseEditCell<T> : ComponentBase
    {
        [Parameter]
        public T Item { get; set; }
        [Parameter]
        public AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass Column { get; set; }
        [Parameter]
        public Action<T, AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass, object> OnModifyItem { get; set; }

        public string WarningCSS { get; set; } = "d-none";
        public string WarningTitle { get; set; }

        public void ShowWarningIcon()
        {
            WarningCSS = String.Empty;
            WarningTitle = String.Concat(Column.ColumnName, ": ",
                String.IsNullOrEmpty(Column.Restrictions.Hint)
                 ? "Incorrect field value"
                 : Column.Restrictions.Hint);
        }

        protected void ApplyModificationToDTO(object newValue)
        {
            //Since there is no two-way bounding to DTO, it modifies the DTO field in code.
            //This should be called from anywhere after validation is successful
            if (newValue == null) newValue = "";
            System.Reflection.PropertyInfo pi = Item.GetType().GetProperty(Column.FieldName);
            if (pi != null)
                switch (pi.PropertyType.Name)
                {
                    case "Int32":
                        int i = 0;
                        int.TryParse(newValue.ToString(), out i);
                        pi.SetValue(Item, i);
                        break;
                    case "DateTime":
                        DateTime DT = DateTime.MinValue;
                        DateTime.TryParse(newValue.ToString(), out DT);
                        pi.SetValue(Item, DT);
                        break;
                    case "Double":
                    case "Float":
                        double d = 0;
                        double.TryParse(newValue.ToString(), out d);
                        pi.SetValue(Item, d);
                        break;
                    default:
                        pi.SetValue(Item, newValue);
                        break;
                }
        }

    }
}

