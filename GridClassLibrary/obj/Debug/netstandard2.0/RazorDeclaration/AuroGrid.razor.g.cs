#pragma checksum "C:\Development\BlazorTest\BlazorGrid\GridClassLibrary\AuroGrid.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "88b22909fc137ff82d7b382b8a11b45d62fd50b3"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace AuroGridClassLibrary
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#line 1 "C:\Development\BlazorTest\BlazorGrid\GridClassLibrary\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
    public partial class AuroGrid<TItem> : AuroGridClassLibrary.Classes.AuroGridBaseClass<TItem>
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#line 423 "C:\Development\BlazorTest\BlazorGrid\GridClassLibrary\AuroGrid.razor"
       
    [Parameter]
    public TItem Item { get; set; }
    [Parameter]
    public Action<TItem, AuroGridClassLibrary.Classes.AuroGridColumnPropertyClass, object> OnModifyItem { get; set; }

    private string RWEDPanelClass;
    private string RWEDFullClass;

    int counter = 0;


    protected override Task OnInitializedAsync()
    {
        if (SecurityAccessLevel <= Classes.SecurityAccessLevels.ViewOnly) RWEDPanelClass = "d-none";
        else if (SecurityAccessLevel < Classes.SecurityAccessLevels.FullAccess) RWEDFullClass = "d-none";
        return base.OnInitializedAsync();
    }

#line default
#line hidden
    }
}
#pragma warning restore 1591
