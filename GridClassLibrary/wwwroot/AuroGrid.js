﻿//--------------Nick Renziglov 2020------------------------
function scrollRowIntoView(rowHashCode)
{
    var row = $('#' + rowHashCode);
    if (row.length>0) row[0].scrollIntoView(true);
}

function showFieldVisibilityDialog() {
    $("#visibilityModal").modal();
}
function hideFieldVisibilityDialog() {
    $('#visibilityModal').modal('hide');
}
function showAddNewRowDialog() {
    $("#addNewRowModal").modal();
}
function hideAddNewRowDialog() {
    $('#addNewRowModal').modal('hide');
}

//function selectDataGridRow(tableId, rowId, cssclass, delay) {
//    //Deselect all rows for this table first
//    $('#' + tableId + ' tr').removeClass(cssclass);

//    //Now select a given one with delay to let browser finish rendering
//    if (rowId !== '')
//        setTimeout(function ()
//        {
//            var v = $("#" + rowId).addClass(cssclass);
//        }, delay);
    
//}

//function strikeoutDataGridRow(tableId, rowId, isOn, delay) {
//    //Do with delay to let browser finish rendering
//    if (rowId !== '')
//        setTimeout(function () { 
//            if (isOn) $("#" + rowId).addClass('strikeout');
//            else $("#" + rowId).removeClass('strikeout');
//        }, delay);

//}