﻿using DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceLibrary1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        private List<string> firstNames = new List<string>()
    {
        "34A55C8E", "D32BB395", "A55D81CE", "C5E63341", "B214E581", "6C25AC1E", "BB7124D7", "A99F9571", "048AE3EF", "4BE74602",
        "C8C22A30", "6FEDC433", "4395F072", "015CF98B", "74787ABA", "6FC91395", "63F98387", "0CC27CFD", "014BF75C", "03E5EB80",
        "1FCE6F4C", "204F24C3", "484F89AB", "CC996C1C", "A1D56D80", "CE79CDBD", "5570C348", "05C1E106", "FC3B150A", "DC027C5F",
        "43C0E1A0", "FD4374E4", "553A75A8", "65D84F69", "EE3A83A0", "4D623394", "39D2F8B4", "D454806C", "A82FA911", "60756FAC",
        "D1496E15", "5AFFCF84", "243EF81C", "4E5B6E66", "07643FBC", "25D85D6F", "F2D1690D", "18AF3383", "4C335F6E", "D701A74A",
        "0547697D", "77F6A425", "A3364BE0", "3F828450", "91296D3C", "B817AD1C", "F6BE024D", "B77394E8", "0A6E5C2D", "104666BC",
        "CE5A50D1", "FD2D99CE", "83A38086", "4214D9CB", "D8875DC4", "F0112AA4", "C7CA6F8A", "C83D51BC", "FFBE404A", "BDA44262",
        "4E20871C", "86AFB387", "6902136B", "CDAE63D4", "833FF576", "AE57D0D1", "EA3E1D67", "C35E5CA8", "2A83E423", "A5F875CD",
        "40970851", "C35B3E37", "64E12382", "D0C48A9C", "74817932", "06B67863", "16CFA606", "7DC5DF25", "82A075B9", "59DC20D7",
        "6486C9A0", "F40BDDE0", "BFA7BB61", "EA0B0EF0", "19D16194", "29CFB6AB", "50CCDB19", "FA27DFF8", "250E7061", "F8D83BA2"
    };
        private List<string> lastNames = new List<string>()
    {
        "59AF607A", "7CBD1B7F", "06652A35", "B8FB408C", "958C4140", "B4875D0F", "9BA8954D", "BA37E163", "F72FC7B2", "F57FAAD1",
        "BFC4D3D2", "37226DAF", "9F24D0FB", "B8211D63", "EB714F4B", "63F21558", "B04EE4EF", "D2CACD7B", "2ADAAD02", "CA1CD73D",
        "60AC785C", "EB4DC435", "3B45E015", "FB5EC394", "CED01024", "FFCF5CBF", "0386403F", "688651BF", "4ABF1B28", "013B3FAC",
        "FD483D6F", "B0E18E50", "0BFE623C", "929A04AD", "AB0768CB", "EB0F2827", "E540C114", "DBE6B927", "57D9CCDE", "85ACB144",
        "FD8D956E", "73CD10BD", "B522339A", "591902C2", "C9681624", "427BE5D5", "A8992909", "33682194", "86512774", "A26E5FED",
        "0D305FD8", "FB88CC48", "0FCA4F50", "52C7222F", "868CFF4C", "A113FD54", "FCCBD2E7", "F28DC541", "F57C90C5", "BD6C630B",
        "491917D4", "0E88107A", "BC9832BE", "3EA03F33", "F4199EB8", "B727596B", "ED307D90", "5D37BF08", "36685E8A", "8F17D4A6",
        "885744E8", "A724C838", "8A4E18B0", "310C70AF", "4D14265F", "08ABEC15", "EB0EEDB9", "7DF7026B", "5A37C88F", "EFAADB9A",
        "174D1F60", "DE34AD6D", "37412043", "14AA3D2F", "3C905EF7", "2B581F14", "43A259A8", "BDB26210", "AF0BB150", "28068528",
        "242FA009", "90A59C8D", "93266A89", "DD502A31", "A5D3EB5B", "4DACF4F2", "C6952BB8", "105DB28E", "F6BDC5B4", "400EB66B",
    };
        public List<EmployeeDTO> GetEmployeeList()
        {
            List<EmployeeDTO> EmployeeList = new List<EmployeeDTO>();
            Random rnd = new Random();
            for (int i = 0; i < 100; i++)
            {
                EmployeeList.Add(new EmployeeDTO()
                {
                    DOB = new DateTime(Convert.ToDateTime("1/1/1980").AddDays(rnd.Next(10000)).Ticks),
                    FirstName = firstNames[i],
                    LastName = lastNames[i],
                    EmployeeNumber = rnd.Next(10000).ToString().PadLeft(5, '0'),
                    Rate = (rnd.Next(10) % 2 == 0) ? 0 : rnd.NextDouble() * 10,
                    RecId = i + 1,
                    Department = rnd.Next(5) + 1,
                    IsTemporary = rnd.Next(10) > 5,
                    TC = String.Concat("C", rnd.Next(13) + 1),
                    BankDays = (rnd.Next(10) % 2 == 0) ? -rnd.Next(100) : rnd.Next(100),
                    ShiftStartTime = DateTime.Now.AddDays(rnd.Next(10)).Date.AddMinutes(rnd.Next(1400)),
                    Email = String.Concat(firstNames[i], "@", lastNames[i], ".com")
                });
            }
            return EmployeeList;
        }

    }
}
